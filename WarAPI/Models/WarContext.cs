﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WarAPI.Models
{
    public class WarContext : DbContext
    {
        public WarContext(DbContextOptions<WarContext> options): base(options)
        {
            
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
        }

        //public WarContext(DbContextOptions<WarContext> options)
        //    : base(options)
        //{
        //    Database.EnsureCreated();
        //}

        public DbSet<User> UserItem { get; set; }
        public DbSet<Product> ProductItem { get; set; }
        public DbSet<Order> OrderItem { get; set; }
    }
}
