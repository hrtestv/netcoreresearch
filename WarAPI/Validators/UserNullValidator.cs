﻿using FluentValidation;
using WarAPI.Models;

namespace WarAPI.Validation
{
    public class UserNullValidator : AbstractValidator<User>
    {
        public UserNullValidator()
        {
            RuleFor(warItem => warItem.Name).NotNull().WithMessage("Имя не может быть пустое!");
        }
    }
}
